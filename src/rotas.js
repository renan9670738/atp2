import {BrowserRouter, Route, Routes} from "react-router-dom"
import Login from "./paginas/Login"
import Cadastro from "./paginas/Cadastro"
import Principal from "./paginas/Principal"

const Rotas = () => {
        return (
            <BrowserRouter>
                <Routes>
                    <Route exact={true} path="/Cadastro" element={<Cadastro/>} />
                    <Route exact={true} path="/" element={<Login/>} />
                    <Route exact={true} path="/Principal" element={<Principal/>} />
                </Routes>
            </BrowserRouter>
        )
    }

export default Rotas