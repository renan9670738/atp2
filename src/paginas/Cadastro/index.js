import React, { Component } from "react";
import { Link } from "react-router-dom"
import firebase from "../../Firebase";


class Cadastro extends Component{

    constructor(props){
        super(props)

        this.state = {
            nome: "nome", 
            email: "email",
            senha: "senha",
            sobrenome:"sobrenome",
            data:"data"

        }
        this.gravar= this.gravar.bind(this)
    }

    async gravar(){

        await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
        .then(async (retorno) => {
            await firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                nome: this.state.nome,
                sobrenome: this.state.sobrenome,
                data: this.state.data
            })
        })

        

        // firebase.firestore().collection("usuario").add({
        //     nome: this.state.nome,
        //     email: this.state.email,
        //     senha: this.state.senha,
        //     sobrenome: this.state.sobrenome,
        //     data: this.state.data,
        // })
    }

    
    render(){
        return(
            <div>
                <h1>Página de Cadastro</h1>
                    <input type="text" placeholder="E-mail" onChange={(e)=>this.setState({email: e.target.value})}></input>
                    <br/>
                    <input type="text" placeholder="Senha" onChange={(e)=>this.setState({senha: e.target.value})}></input>
                    <br/>
                    <input type="text" placeholder="Nome" onChange={(e)=>this.setState({nome: e.target.value})}></input>
                    <br/>
                    <input type="text" placeholder="Sobrenome" onChange={(e)=>this.setState({sobrenome: e.target.value})}></input>
                    <br/>
                    <input type="date" placeholder="Data de nascimento" onChange={(e)=>this.setState({data: e.target.value})}></input>
                    <br/>
                    <button onClick={this.gravar}>Cadastrar</button>

                    <Link to="/">
                        <button>Fazer login</button>
                    </Link>
            </div>
        )
    }
}

export default Cadastro