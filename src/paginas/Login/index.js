import React, { Component } from "react";
import { Link } from "react-router-dom"
import firebase from "../../Firebase"

class Login extends Component{

    constructor(props){
        super(props)

        this.state = {
            email: "",
            senha: "",
        
        };

        this.logar = this.logar.bind(this)
    }
    
    async logar(){
    
        await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.senha)
        .then(()=>{
            window.location.href = "./principal"
        })
        .catch((erro)=>{
            console.log("Erro ao acessar conta")
            return(alert("Usuário não cadastrado"))
            
        })
    }

    render(){
        return(
            <div >
                <h1>Login</h1>
                <input type="text" placeholder="E-mail" onChange={(e)=>this.setState({email: e.target.value})}></input>
                <br/>
                <input type="text" placeholder="Senha" onChange={(e)=>this.setState({senha: e.target.value})}></input>
                <br/>
                <button onClick={this.logar}>Acessar conta</button>

                
                <Link to="/Cadastro">
                    <button>Voltar para Cadastro</button>
                </Link>
            </div>
        );
    }
}

export default Login           
