import React, { Component } from "react";
// import { Link } from "react-router-dom"

class Login extends Component{

    constructor(props){
        super(props)

        this.state = {
            titulo: "Login", 
            label: ""

        
        };

        // this.mudar = this.mudar.bind(this)
        this.logar = this.logar.bind(this)
    }
    
    // mudar(){
    //     var novoTitulo = "Novo titulo"
    //     this.setState({titulo:novoTitulo})
    // }

    logar(){
    
        var email = document.getElementById("email").value;
        var senha = document.getElementById("senha").value;
    
        if (email == "eduardo.lino@pucpr.br" && senha == "123456"){
            var novaLabel = "Acessado com sucesso!"
            this.setState({label:novaLabel})
        }
        else{
            var novaLabel = "Usuário ou senha incorretos!"
            this.setState({label:novaLabel})
        }
    }

    


    render(){
        return(
            <div >
                <h1>{this.state.titulo}</h1>
                {/* { <button onClick={this.mudar}> Mudar titulo </button>} */}
                <form>
                    <input type="text" placeholder="E-mail" id="email"></input><br/>
                    <input type="text" placeholder="Senha" id="senha"></input><br/>
                    <button type="submit" onClick={this.logar}>Acessar conta</button>
                    <br/>
                    <label>{this.state.label}</label>
                </form>
            </div>
        );
    }
}

export default Login           
